# Test GC performance of nodejs, python, java


method: time creating trees with NCHILDREN children at each node and NTREEDEPTH. 

performance (sec)

(Children, Depth) |    c++      |  java       |   python   | nodejs
------------------|-------------|-------------|------------|------------------
  (5,10)          |-------------| 1.6, 3      | -          |
  (5,8)           |    4.5      | 0.075,0.150 | 6.2, 7.2   |  -
  (5,7)           |-------------|             |            | 1.3, 1.7
