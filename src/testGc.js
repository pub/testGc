var moment = require('moment');

var NCHILDREN = 5;
var NTREEDEPTH = 7;
var NDATAREPEAT = 8;

function TreeNode(data) {
    this.children = [];
    this.data = data;
    this.parent = null;
}
TreeNode.prototype = {
    addChild: function(child)  {
        this.children.push(child);
        child.parent = this;
        return this;
    },
}

function createChildren(parent) {
    for(var i=0; i < NCHILDREN; ++i) {
        var a = [];
        for(var k=0; k < NDATAREPEAT; ++k) {
//            a.push("123456789ABCDEF".repeat(NDATAREPEAT));
            a.push("123456789ABCDEF");
        }
        parent.addChild(new TreeNode(a));
    }
    return parent.children;
}

function createTree() {
    var roots = [ new TreeNode() ];
    for(var i=0; i<NTREEDEPTH; ++i) {
        var subtrees = [];
        for(var j=0; j<roots.length; ++j) {
            subtrees = subtrees.concat( createChildren(roots[j]) ); 
        }
        roots = subtrees;
    }
}

function createForest(n) {
//    setTimeout ( function() {
//        createTree();
    var n = 0;
    var prevTime = moment();
    while( true) {
        createTree();
//        createForest(n+1);
        var currTime = moment();
        console.log(n, "js:createForest!", currTime.diff(prevTime));
        prevTime = currTime;
        ++n;
    }
//    }, 0);
}

createForest(0);
