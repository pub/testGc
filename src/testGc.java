//package jz;

import java.util.*;
import java.sql.Date;
import java.sql.Timestamp;

class TestGc {

    final static int NCHILDREN = 5;
    final static int NTREEDEPTH = 10;
    final static int NDATAREPEAT = 8;

    static String repeat(String s, int n) {
        final StringBuilder sb = new StringBuilder();
        for(int i = 0; i < n; i++) {
            sb.append(s);
        }
        return sb.toString();
    }
    static List<TreeNode> createChildren(TreeNode parent) {
        for(int i=0; i<NCHILDREN; ++i) {
/*            List<String> a = new LinkedList();
            for(int j=0; j< NDATAREPEAT; ++j) {
                a.add("123456789ABCDEF");
            }
            */
//            parent.addChild( new TreeNode((String[])a.toArray(new String[NDATAREPEAT])) );
            parent.addChild( new TreeNode(null) );
        }
        return parent.children;
    }

    static void createTree() {
        List<TreeNode> roots = new LinkedList<TreeNode>();
        roots.add( new TreeNode(null) );
        for(int i=0; i<NTREEDEPTH; ++i) {
            List<TreeNode> subtrees = new LinkedList<TreeNode>();
            for(TreeNode e : roots) {
                subtrees.addAll(createChildren(e));
            }
            roots = subtrees;
        }
    }

    static void recursiveCreateTree(int DEPTH, TreeNode root) {
        if( DEPTH <=0 ) return;
        if( root == null ) {
            root = new TreeNode(null);
        }
        List<TreeNode> children = createChildren(root);
        if( DEPTH == 1 ) return;
        for(TreeNode child : children) {
            recursiveCreateTree(DEPTH-1, child);
        }
    }

    static void createForest() {
        Timestamp prevTime = new Timestamp(new java.util.Date().getTime());
        for(long n=0; ; ++n) {
//            createTree();
            recursiveCreateTree(NTREEDEPTH, null);
            Timestamp currTime = new Timestamp(new java.util.Date().getTime());
            System.out.println(n + " java:Created Forest: " + (currTime.getTime() - prevTime.getTime()));
            prevTime = currTime;
        }
    }
    public static void main(String[] args) {
        createForest();
    }
}

class TreeNode {
    public TreeNode(String[] data) {
        this.data = data;
        this.children = new LinkedList<TreeNode>();
        this.parent = null;
    }

    public TreeNode addChild(TreeNode child) {
        this.children.add(child);
        this.parent = this;
        return this;
    }

    public String[] data;
    public List<TreeNode> children;
    public TreeNode parent;
}