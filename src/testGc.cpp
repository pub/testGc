#include <list>
#include <vector>
#include <vector>
#include <iostream>
#include <memory>
#include <boost/pool/pool.hpp>
#include <boost/thread.hpp>

const int NCHILDREN = 5;
const int NTREEDEPTH = 10;
const int NDATAREPEAT = 8;

template<typename T, typename Allocator = std::allocator<T> >
struct pooled_allocator: public std::allocator<T> {
    typedef pooled_allocator ThisType;
    typedef T* pointer;
    typedef const T* const_pointer;
    typedef void value_type;
    typedef size_t size_type;
    template <class U> struct rebind { typedef pooled_allocator<U> other; };

    typedef std::vector<void *> PointerList;
    typedef std::shared_ptr<PointerList> PointerListPtr;

    PointerListPtr d_allocated;
    pooled_allocator() 
        : d_allocated( std::make_shared<PointerList>() )
    {
//        std::cout << " pooled_allocator() " << std::endl ;
    };

    template<typename U>
    pooled_allocator(const pooled_allocator<U>& other)
        :d_allocated(other.d_allocated)
    {
//        std::cout << " pooled_allocator:cp_ctor,existing:"<<d_allocated->size() << std::endl;
    };

    pointer allocate(size_t n, const void *hint=0) {
        my_pool::malloc();
        if( n>1 ) {
            std::cerr << "ERROR! pooled_allocator: alloating array is not supported!" << std::endl;
            return pointer();
        }
        if( d_allocated->size() == 0 ) {
            return static_cast<pointer>( Allocator::allocate(n));
        }else{
            pointer ret = (pointer)d_allocated->back();
            d_allocated->pop_back();
            return ret;
        }
    }

    void deallocate(pointer p, size_type n) {
        // todo validate p
        d_allocated->push_back(p);
    }
    size_t purge_memory() {
        size_t ret = d_allocated->size();
        // todo
        return ret;
    }
};

template<typename T>
std::shared_ptr<T> pooled_create() {
    static pooled_allocator<T> *alloc = new pooled_allocator<T>();
//    static boost::pool_allocator<T> *alloc = new boost::pool_allocator<T>();
//    static std::allocator<T> *alloc = new std::allocator<T>();
//    return std::allocate_shared<T>(*alloc);
    return std::make_shared<T>();
}

class TreeNode;
typedef std::shared_ptr<TreeNode> TreeNodePtr;
typedef std::list<TreeNodePtr>   TreeNodePtrList;
typedef std::shared_ptr<TreeNodePtrList> TreeNodePtrListPtr;
typedef std::vector<std::string> StringList;

struct TreeNode {

    TreeNode* parent;
    TreeNodePtrList children;
    StringList data;

    TreeNode():parent(NULL) {
    }
    TreeNode(const StringList data)
        :parent(NULL), data(data) {
    }
    void addChild(TreeNodePtr child) {
        children.push_back(child);
        child->parent = this;
    }
    ~TreeNode() {
/*        for(TreeNodePtrList::iterator it=children.begin(); it!=children.end(); ++it) {
            if( *it ) {
                (*it)->parent = NULL;
            }
        }
        */
    }
};

std::allocator<TreeNode> alloc;
TreeNodePtrList& createChildren(TreeNodePtr parent) {
    for(int i=0; i<NCHILDREN; ++i) {
/*        StringList a;
        for(int j=0; j<NDATAREPEAT; ++j) {
            a.push_back("123456789ABCDEF");
        }
        */
//        parent->addChild(std::make_shared<TreeNode>());
        parent->addChild(pooled_create<TreeNode>());
    }
    return parent->children;
}

void createTree() {
    TreeNodePtrListPtr roots = TreeNodePtrListPtr( new TreeNodePtrList() );
    TreeNodePtr root = pooled_create<TreeNode>();
    roots->push_back( root );
    for(int i=0; i<NTREEDEPTH; ++i) {
        std::cout << " createTree:depth:" << i << std::endl;
        TreeNodePtrListPtr subtrees = TreeNodePtrListPtr( new TreeNodePtrList() );
        for(TreeNodePtrList::iterator it=roots->begin(); it!=roots->end(); ++it) {
            TreeNodePtrList& children = createChildren(*it);
            subtrees->insert(subtrees->end(), children.begin(), children.end());
        }
        roots = subtrees;
    }
}

void recursiveCreateTree(const int DEPTH=NTREEDEPTH, TreeNodePtr root=TreeNodePtr()){
    if( DEPTH <=0 ) return;

    if( !root ) {
        root = pooled_create<TreeNode>();
    }
    TreeNodePtrList& children = createChildren(root);
    if( DEPTH == 1 ) return;
    for(TreeNodePtrList::iterator it=children.begin(); it!=children.end(); ++it) {
        recursiveCreateTree(DEPTH-1, *it);
    }
}

void createForest() {
    clock_t prevTime = clock();
    for(int n=0; ; ++n) {
//        createTree();
        recursiveCreateTree();
        clock_t currTime = clock();
        std::cout << n << " createForest " << (float)(currTime - prevTime)/CLOCKS_PER_SEC << std::endl;
        prevTime = currTime;
    }
}

int main() {
    createForest();
    return 0;
}