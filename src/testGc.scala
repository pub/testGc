import scala.collection.JavaConversions._
class TreeNode(data:Array[String], a_children:java.util.LinkedList[TreeNode], a_parent:TreeNode) {
    var parent = a_parent
    var children = a_children
    def this(data:Array[String]) = this(data, new java.util.LinkedList[TreeNode](), null)
    def this() = this(null)
    def addChild(child: TreeNode) = {
        children.add(child) // child :: children//scala.collection.mutable.LinkedList(child)
        child.parent = this
        children
    }
}
object TestScala {

val NCHILDREN = 5
val NTREEDEPTH = 10
val NDATAREPEAT = 8

def createChildren(root: TreeNode) = {
    for(i <- 0 until NCHILDREN) 
        root.addChild( new TreeNode() )
    root.children
}

def createTree() {
    var roots = new java.util.LinkedList[TreeNode]() //new TreeNode() :: Nil //scala.collection.mutable.LinkedList[TreeNode]( new TreeNode() )
    roots.add(new TreeNode())
    for(i <- 0 until NTREEDEPTH) {
        var subtrees = new java.util.LinkedList[TreeNode]() // Nil //scala.collection.mutable.LinkedList[TreeNode]()
        for(e <- roots) {
            subtrees.addAll(createChildren(e))//  subtrees = subtrees ++ createChildren(e)
        }
        roots = subtrees
    }
}
def recursiveCreateTree(DEPTH:Int, a_root: TreeNode) {
        if( DEPTH <=0 ) return;
        var root = a_root;
        if( root == null ) {
            root = new TreeNode(null);
        }
        val children = createChildren(root);
        if( DEPTH == 1 ) return;
        for(child <- children) {
            recursiveCreateTree(DEPTH-1, child);
        }
    }


def createForest() {
    var prevTime = new java.sql.Timestamp(new java.util.Date().getTime())
    for(i <- 0 until 100000) {
//        createTree()
        recursiveCreateTree(NTREEDEPTH, null)
        val currTime = new java.sql.Timestamp(new java.util.Date().getTime())
        println("createdTree:" + i + " time:" + (currTime.getTime() - prevTime.getTime()) )
        prevTime = currTime
    }
}
def main(args:Array[String]) {
    createForest()
}
}