import time

NCHILDREN = 5
NTREEDEPTH = 10
NDATAREPEAT = 8

class TreeNode:
    def __init__(self, data):
        self.children = []
        self.data = data
        self.parent = None

    def addChild(self, child):
        self.children.append(child)
        child.parent = self


def createChildren(parent):
    for i in range(NCHILDREN):
        a = []
        for j in range(NDATAREPEAT):
#            a.append("123456789ABCDEF"*NDATAREPEAT)
            a.append("123456789ABCDEF")
        parent.addChild( TreeNode(a) )
    return parent.children

def createTree():
    roots = [ TreeNode(None) ]
    for i in range(NTREEDEPTH):
        subtrees = []
        for j in roots:
            subtrees += createChildren(j)
        roots = subtrees

def createForest():
    n = 0
    prevTime = time.clock();
    while True:
        createTree()
#        time.sleep(0.001)
        currTime = time.clock();
        print n, " py:createdForest ", currTime - prevTime;
        prevTime = currTime;
        n += 1

if __name__ == "__main__":
    createForest()